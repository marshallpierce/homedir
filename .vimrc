set nocompatible                                " Ditch strict vi compatibility
set backspace=indent,eol,start  " More powerful backspacing

syntax on

set incsearch          " Incremental search
set autoindent                  " always set autoindenting on
set textwidth=0                 " Don't wrap words by default
set nobackup                    " Don't keep a backup file
set ruler                               " show the cursor position all the time
set showcmd                     " Show (partial) command in status line.
set showmatch           " Show matching brackets.
set ignorecase          " Do case insensitive matching
set autowrite           " Automatically save before commands like :next and :make
set hlsearch        " Highlight search matches
set wildmenu        " Use BASH style completion
set wildmode=list:longest
set history=50                  " keep 50 lines of command line history
set viminfo='20,\"50    " read/write a .viminfo file, don't store more than
                                                " 50 lines of registers
                                                
"make tabs be spaces instead
set expandtab
set softtabstop=4
set tabstop=4
set shiftwidth=4

" make requires real tabs
autocmd FileType make set noexpandtab shiftwidth=8

" don't do the stupid "Thanks for flying Vim"
set notitle

set background=dark

filetype plugin indent on

