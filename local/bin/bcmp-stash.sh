#!/bin/sh

if [ "$#" -gt "0" ]; then
    bench_filter="$1"
else
    bench_filter=""
fi

set -ex

mkdir -p tmp

cargo +nightly check --benches
git stash

cargo +nightly bench $BENCH_ARGS $bench_filter > tmp/control.bcmp

git stash pop

cargo +nightly bench $BENCH_ARGS $bench_filter > tmp/branch.bcmp

cargo benchcmp tmp/control.bcmp tmp/branch.bcmp

