#!/bin/sh

set -e

if [[ "$#" != "1" ]]; then
    echo "Usage: $0 branch_name"
    exit 1
fi

parent_branch="${1}"

branches=`git branch --merged ${parent_branch} | grep -vE '(.* -> .*|develop$|master$)' | sed -e 's/^ +//'`

echo "Branches:"
for b in `echo $branches | tr ' ' '\n'`; do
    echo "$b"
    echo -n "    "
    echo -e `git show --pretty=format:"%Cgreen%ci %Cblue%cr %C(auto)%h %s%Creset" refs/heads/$b | head -n 1`
done

read -p "Delete all these? (y/n) " confirmation

if [[ "$confirmation" != 'y' ]]; then
    echo "OK, aborting"
    exit
fi

echo "Deleting all the things!"

for l in `echo $branches | tr ' ' '\n'` ; do
    git branch -D $l
done
