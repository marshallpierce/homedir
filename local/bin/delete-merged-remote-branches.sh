#!/bin/sh

set -e

if [[ "$#" != "2" ]]; then
    echo "Usage: $0 remote_name branch_name"
    exit 1
fi

remote=$1
parent_branch="${remote}/${2}"

branches=`git branch -r --merged ${parent_branch} | grep -vE '(.* -> .*|develop$|master$)' | sed -e 's/^ +//'`

echo "Branches:"
for b in `echo $branches | tr ' ' '\n'`; do
    echo "$b"
    echo -n "    "
    echo -e `git show --pretty=format:"%Cgreen%ci %Cblue%cr %C(auto)%h %s%Creset" $b | head -n 1`
done

read -p "Delete all these? (y/n) " confirmation

if [[ "$confirmation" != 'y' ]]; then
    echo "OK, aborting"
    exit
fi

echo "Deleting all the things!"

for l in `echo $branches | tr ' ' '\n'` ; do
    br_remote=`echo $l | sed -re 's!^(\w+)/.*!\1!'`
    br=`echo $l | sed -re 's!^\w+/(.*)!\1!'`

    if [[ "$br_remote" != "$remote" ]]; then
        echo "Skipping branch ${l} with different remote"
        continue
    fi

    git push --delete $remote $br

done
